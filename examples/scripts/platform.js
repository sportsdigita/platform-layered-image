var data = {
    backgroundImageUrl: 'images/background.jpg',
    logos: [
        { "src": "images/packerhelmet.png" },
        { "src": "images/CocaCola.jpg", "hotspotOrientationId": "2" }
    ],
    hotspots: [{
            "bgColor": "#22324b",
            "alpha": 0.4,
            "d0x": 43,
            "d0y": 234,
            "d1x": 261,
            "d1y": 226,
            "d2x": 262,
            "d2y": 310,
            "d3x": 47,
            "d3y": 316
        },
        {
            "replicate": true,
            "maxRepeatHorizontal": 3,
            "maxRepeatVertical": 2,
            "bgColor": "#116395",
            "zIndex": 1,
            "preferredOrientation": "2",
            "d0x": 287,
            "d0y": 228,
            "d1x": 693,
            "d1y": 221,
            "d2x": 693,
            "d2y": 308,
            "d3x": 287,
            "d3y": 313
        },
        {
            "bgColor": "#116395",
            "alpha": 1,
            "zIndex": 1,
            "d0x": 699,
            "d0y": 219,
            "d1x": 983,
            "d1y": 219,
            "d2x": 983,
            "d2y": 308,
            "d3x": 699,
            "d3y": 308
        },
        {
            "src": 'images/players-boards.png',
            "x": 22,
            "y": 203,
            "zIndex": 2,
            "width": 961,
            "height": 131
        }
    ]
};

var layeredImage = new PlatformLayeredImage({
    backgroundImageUrl: data.backgroundImageUrl,
    logos: data.logos,
    hotspots: data.hotspots,
    canvasId: 'contextCanvas'
});