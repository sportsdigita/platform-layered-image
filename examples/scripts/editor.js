var exampleData = {
    backgroundImage: 'images/background.jpg',
    logos: [
        { "image": "images/packerhelmet.png" },
        { "image": "images/CocaCola.jpg", "hotspotOrientationId": "58503b80dfb654e46377f028", "name": "Portrait" },
        { "image": "images/ducks.png", "hotspotOrientationId": "58503bdbdfb654e46377f029", "name": "Landscape" }
    ],
    hotspots: [{
            "bgColor": "#22324b",
            "alpha": 0.7,
            "padding": 10,
            "d0x": 43,
            "d0y": 234,
            "d1x": 261,
            "d1y": 226,
            "d2x": 262,
            "d2y": 310,
            "d3x": 47,
            "d3y": 316,
            "zIndex": 1
        },
        {
            "preferredOrientation": "58503b80dfb654e46377f028",
            "replicate": true,
            "maxRepeatHorizontal": 3,
            "maxRepeatVertical": 2,
            "padding": 10,
            "d0x": 287,
            "d0y": 228,
            "d1x": 693,
            "d1y": 221,
            "d2x": 693,
            "d2y": 308,
            "d3x": 287,
            "d3y": 313,
            "zIndex": 1
        },
        {
            "preferredOrientation": "58503bdbdfb654e46377f029",
            "bgColor": "#116395",
            "alpha": 1,
            "padding": 10,
            "d0x": 699,
            "d0y": 219,
            "d1x": 983,
            "d1y": 219,
            "d2x": 983,
            "d2y": 308,
            "d3x": 699,
            "d3y": 308,
            "zIndex": 1
        },
        {
            "image": 'images/players-boards.png',
            "x": 22,
            "y": 203,
            "width": 961,
            "height": 131,
            "zIndex": 2
        }
    ]
};
var editor = ace.edit("codeEditor");
editor.setTheme("ace/theme/twilight");
editor.session.setMode("ace/mode/javascript");

var data, layeredImage;
var savedData = localStorage.getItem('savedData');
if (savedData) {
    // $('#codeInput').val(savedData);
    try {
        editor.setValue(savedData);
        data = JSON.parse(savedData);
    } catch (err) {
        data = exampleData;
        editor.setValue(JSON.stringify(data, null, '\t'));
    }
} else {
    data = exampleData;
    // $('#codeInput').val(JSON.stringify(data, null, '\t'));
    editor.setValue(JSON.stringify(data, null, '\t'));
}

$('#updateRenderingButton').click(function() {
    // var d = JSON.parse($('#codeInput').val());
    var d = editor.getValue();
    localStorage.setItem('savedData', d);
    data = JSON.parse(d);
    loadRendering(data);
});

$('#loadExampleButton').click(function() {
    data = exampleData;
    localStorage.setItem('savedData', JSON.stringify(data, null, '\t'));
    // $('#codeInput').val(JSON.stringify(d, null, '\t'));
    editor.setValue(JSON.stringify(data, null, '\t'));
    loadRendering(data);
});


$('#hideSelectedHotspot').click(function() {
    var hotspot = layeredImage.getSelectedHotspot();

    if (hotspot) {
        layeredImage.hideHotspot(hotspot);
    }
});

$('#showAllHotspots').click(function() {
    layeredImage.showAllHotspots();
});

$('#hideAllHotspots').click(function() {
    layeredImage.hideAllHotspots();
});

$('#zoom2x').click(function() {
    layeredImage.setScale(2);
});

$('#zoom1x').click(function() {
    layeredImage.setScale(1);
});

$('#zoomHalfx').click(function() {
    layeredImage.setScale(0.5);
});

$('#toggleSkew').click(function() {
    var selectedHotspot = layeredImage.getSelectedHotspot();

    if (!selectedHotspot)
        return;

    if (layeredImage.isSkewedLayer(selectedHotspot)) {
        layeredImage.convertFromSkew(selectedHotspot);
    } else {
        layeredImage.convertToSkew(selectedHotspot);
    }
});

$('#changeLogo').click(function() {
    var selectedHotspot = layeredImage.getSelectedHotspot();

    if (!selectedHotspot)
        return;

    selectedHotspot.preferredOrientation = "58503b80dfb654e46377f028";

    layeredImage.hotspotLogoUpdated(selectedHotspot);
});
$('#deleteSelectedHotspot').click(function() {
    var hotspot = layeredImage.getSelectedHotspot();

    if (hotspot) {
        layeredImage.deleteHotspot(hotspot);
        refreshEditor();
    }
});

function refreshEditor() {
    editor.setValue(JSON.stringify(data, null, '\t'));
}

new Clipboard('#copyToClipboard', {
    text: function(trigger) {
        //duplicate
        var data = JSON.parse(editor.getValue());
        //remove logos
        delete data.logos;

        var plugin = {
            "id": "590b472f957d5da4048cf6ae",
            "alias": "digideck_slide_static-hotspot-v2",
            "options": data
        };

        // console.log(plugin);
        return JSON.stringify(plugin, null, '\t');
    }
});

// var onRenderListener = function(canvas, context) {
//     var imageData = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
//     $('.editorCanvasImageContainer').css('background-image', 'url(' + imageData + ')');
// };

var loadRendering = function(d) {
    $('.editorCanvasImageContainer').css('background-image', 'url()');

    if (layeredImage) {
        layeredImage.destroy();
    }

    layeredImage = new PlatformLayeredImageEditor({
        backgroundImage: d.backgroundImage,
        logos: d.logos,
        canvasId: "theCanvas",
        hotspots: d.hotspots,
        // onRendered: onRenderListener,
        onHotspotUpdated: function() {
            refreshEditor();
        }
    });

};

loadRendering(data);