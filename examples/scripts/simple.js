var layeredImage = new LayeredImage({ backgroundImageUrl: 'images/background.jpg', canvasId: 'contextCanvas' });

layeredImage.setLayers([{
        "src": "images/packerhelmet.png",
        "bgColor": "#22324b",
        "alpha": 0.4,
        "d0x": 43,
        "d0y": 234,
        "d1x": 261,
        "d1y": 226,
        "d2x": 262,
        "d2y": 310,
        "d3x": 47,
        "d3y": 316
    },
    {
        "src": "images/iTunes.png",
        "replicate": true,
        "maxRepeatHorizontal": 3,
        "maxRepeatVertical": 2,
        "bgColor": "#116395",
        "zIndex": 1,
        "opacity": 0.2,
        "d0x": 287,
        "d0y": 228,
        "d1x": 693,
        "d1y": 221,
        "d2x": 693,
        "d2y": 308,
        "d3x": 287,
        "d3y": 313
    },
    {
        "src": "images/CocaCola.jpg",
        "bgColor": "#116395",
        "alpha": 1,
        "zIndex": 1,
        "d0x": 699,
        "d0y": 219,
        "d1x": 983,
        "d1y": 219,
        "d2x": 983,
        "d2y": 308,
        "d3x": 699,
        "d3y": 308
    },
    {
        "src": "images/CocaCola.jpg",
        "alpha": 1,
        "bgColor": "#116395",
        "zIndex": 3,
        "d0x": 370,
        "d0y": 560,
        "d1x": 460,
        "d1y": 560,
        "d2x": 450,
        "d2y": 610,
        "d3x": 360,
        "d3y": 610
    },
    {
        "src": 'images/players-boards.png',
        "x": 22,
        "y": 203,
        "zIndex": 2,
        "width": 961,
        "height": 131
    }
]);