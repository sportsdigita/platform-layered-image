var exampleData = {
    backgroundImage: 'images/background.jpg',
    logos: [
        { "image": "images/packerhelmet.png" },
        { "image": "images/CocaCola.jpg", "hotspotOrientationId": "58503b80dfb654e46377f028", "name": "Portrait" },
        { "image": "images/ducks.png", "hotspotOrientationId": "58503bdbdfb654e46377f029", "name": "Landscape" }
    ],
    hotspots: [{
        "zIndex": 6,
        "maxRepeatHorizontal": 5,
        "d3y": 209,
        "d3x": 295,
        "d2y": 205,
        "d2x": 701,
        "d1y": 114,
        "d1x": 702,
        "d0y": 119,
        "d0x": 294,
        "replicate": true,
        "alpha": 1,
        "name": "Test"
    }, {
        "zIndex": 4,
        "alpha": 1,
        "width": 426,
        "height": 83,
        "y": 161,
        "x": 317,
        "image": "images/iTunes.png",
        "name": "Stache"
    }]
};

$('#hideHotspot').click(function() {
    if (exampleData.hotspots.length)
        layeredImage.hideHotspot(exampleData.hotspots[(exampleData.hotspots.length - 1)]);
});

$('#showHotspot').click(function() {
    if (exampleData.hotspots.length)
        layeredImage.showHotspot(exampleData.hotspots[(exampleData.hotspots.length - 1)]);
});

$('#showAllHotspots').click(function() {
    layeredImage.showAllHotspots();
});

$('#hideAllHotspots').click(function() {
    layeredImage.hideAllHotspots();
});

$('#addHotspot').click(function() {
    switch (exampleData.hotspots.length) {
        case 0:
            layeredImage.addHotspot({
                "bgColor": "#22324b",
                "alpha": 0.7,
                "padding": 10,
                "d0x": 43,
                "d0y": 234,
                "d1x": 261,
                "d1y": 226,
                "d2x": 262,
                "d2y": 310,
                "d3x": 47,
                "d3y": 316,
                "zIndex": 1
            });
            break;
        case 1:
            layeredImage.addHotspot({
                "preferredOrientation": "58503b80dfb654e46377f028",
                "replicate": true,
                "maxRepeatHorizontal": 3,
                "maxRepeatVertical": 2,
                "bgColor": "#116395",
                "padding": 10,
                "d0x": 287,
                "d0y": 228,
                "d1x": 693,
                "d1y": 221,
                "d2x": 693,
                "d2y": 308,
                "d3x": 287,
                "d3y": 313,
                "zIndex": 1
            });
            break;
        case 2:
            layeredImage.addHotspot({
                "preferredOrientation": "58503bdbdfb654e46377f029",
                "bgColor": "#116395",
                "alpha": 1,
                "padding": 10,
                "d0x": 699,
                "d0y": 219,
                "d1x": 983,
                "d1y": 219,
                "d2x": 983,
                "d2y": 308,
                "d3x": 699,
                "d3y": 308,
                "zIndex": 1
            });
            break;
        case 3:
            layeredImage.addHotspot({
                "image": 'images/players-boards.png',
                "x": 0,
                "y": 0,
                "width": 961,
                "height": 131,
                "zIndex": 2
            });
            break;
    }
});

$('#deleteHotspot').click(function() {
    var hotspot;

    if (exampleData.hotspots.length >= 0) {
        layeredImage.deleteHotspot(exampleData.hotspots[(exampleData.hotspots.length - 1)]);
    }
});

$('#selectHotspot').click(function() {
    var hotspot;

    if (exampleData.hotspots.length >= 0) {
        layeredImage.setSelectedHotspot(exampleData.hotspots[(exampleData.hotspots.length - 1)]);
    }
});

var onRenderListener = function(canvas, context) {

};

var onHotspotSelectedListener = function(hotspot) {
    console.log(hotspot);
};

var loadRendering = function(d) {
    layeredImage = new PlatformLayeredImageEditor({
        backgroundImage: d.backgroundImage,
        logos: d.logos,
        hotspots: d.hotspots,
        canvasId: "theCanvas",
        onRendered: onRenderListener,
        onHotspotSelected: onHotspotSelectedListener
    });
};

loadRendering(exampleData);