(function() {
    var canvas = document.getElementById('contextCanvas'),
        context = canvas.getContext('2d'),
        img1 = new Image(),
        imageLoaded = false,
        forgroundImages = [],
        gridImage = 'images/transp_360.png',
        containerImages = [];

    var editMode = false;
    var si = 0;

    var forgrounds = [{
        src: 'images/players-boards.png',
        x: 22,
        y: 203,
        width: 961,
        height: 131
    }];

    var containers = [{
            "src": "images/Red_Sox.jpg",
            "x": 43,
            "y": 234,
            "bgColor": "#22324b",
            "width": 218,
            "height": 79,
            "d0x": 0,
            "d0y": 0,
            "d1x": 0,
            "d1y": -8,
            "d2x": 1,
            "d2y": -3,
            "d3x": 4,
            "d3y": 3
        },
        {
            "src": "images/iTunes.png",
            "bgColor": "#116395",
            "x": 287,
            "y": 228,
            "width": 406,
            "height": 88,
            "opacity": 0.2,
            "d0x": 0,
            "d0y": 0,
            "d1x": 0,
            "d1y": -7,
            "d2x": 0,
            "d2y": -8,
            "d3x": 0,
            "d3y": -3
        },
        {
            "src": "images/CocaCola.jpg",
            "bgColor": "#116395",
            "x": 699,
            "y": 219,
            "width": 284,
            "height": 89,
            "d0x": 0,
            "d0y": 0,
            "d1x": 0,
            "d1y": 0,
            "d2x": 0,
            "d2y": 0,
            "d3x": 0,
            "d3y": 0
        }
    ];
    // Start listening to resize events and
    // draw canvas.
    initialize();

    function initialize() {
        // window.addEventListener('resize', resizeCanvas, false);

        img1.src = 'images/background.jpg';
        img1.onload = function() {
            imageLoaded = true;
            resizeCanvas();
        };

        $.each(forgrounds, function(index, forground) {
            var img = new Image();
            var forgroundData = { isLoaded: false, data: forground, img: img };
            img.onload = function() {
                forgroundData.isLoaded = true;
                resizeCanvas();
            };
            img.src = forground.src;
            forgroundImages.push(forgroundData);
        });

        $.each(containers, function(index, container) {
            var containerData = { isLoaded: false, data: container };
            containerImages.push(containerData);
            resizeImage(container.src, container.height, function(img) {
                var rimg = new Image();
                rimg.src = (!editMode) ? replicateImage(1, img, container, 1) : gridImage;
                rimg.onload = function() {
                    if (container.bgColor && container.bgColor.length) {
                        var backgroundImage = new Image();
                        backgroundImage.src = createBackgroundImage(container);
                        backgroundImage.onload = function() {
                            containerData.img = rimg;
                            containerData.backgroundImage = backgroundImage;
                            containerData.isLoaded = true;
                            resizeCanvas();
                        };
                    } else {
                        containerData.img = rimg;
                        containerData.isLoaded = true;
                        resizeCanvas();
                    }
                };
            });
        });

        // Draw canvas border for the first time.
        resizeCanvas();
    }

    function redraw() {
        if (imageLoaded) {
            context.save();
            context.drawImage(img1, 0, 0);
            context.restore();
        }

        $.each(containerImages, function(index, containerImage) {
            if (containerImage.isLoaded) {
                if (containerImage.backgroundImage) {
                    var bp = new Perspective(context, containerImage.backgroundImage);
                    bp.draw([
                        [containerImage.data.x + containerImage.data.d0x, containerImage.data.y + containerImage.data.d0y],
                        [(containerImage.data.x + containerImage.data.width) + containerImage.data.d1x, containerImage.data.y + containerImage.data.d1y],
                        [(containerImage.data.x + containerImage.data.width) + containerImage.data.d2x, (containerImage.data.y + containerImage.data.height) + containerImage.data.d2y],
                        [containerImage.data.x + containerImage.data.d3x, (containerImage.data.y + containerImage.data.height) + containerImage.data.d3y]
                    ]);
                }
                var p = new Perspective(context, containerImage.img);
                p.draw([
                    [containerImage.data.x + containerImage.data.d0x, containerImage.data.y + containerImage.data.d0y],
                    [(containerImage.data.x + containerImage.data.width) + containerImage.data.d1x, containerImage.data.y + containerImage.data.d1y],
                    [(containerImage.data.x + containerImage.data.width) + containerImage.data.d2x, (containerImage.data.y + containerImage.data.height) + containerImage.data.d2y],
                    [containerImage.data.x + containerImage.data.d3x, (containerImage.data.y + containerImage.data.height) + containerImage.data.d3y]
                ]);
            }
        });

        $.each(forgroundImages, function(index, forgroundImage) {
            if (forgroundImage.isLoaded) {
                context.save();
                context.drawImage(forgroundImage.img, forgroundImage.data.x, forgroundImage.data.y, forgroundImage.data.width, forgroundImage.data.height);
                context.restore();
            }
        });

        $('#json_output').val(JSON.stringify(containerImages[si].data));
    }

    // Resizes to the background image
    function resizeCanvas() {
        canvas.width = img1.width;
        canvas.height = img1.height;
        redraw();
    }



    var canvasOffset = $("#contextCanvas").offset();
    var offsetX = canvasOffset.left;
    var offsetY = canvasOffset.top;
    var canvasWidth = canvas.width;
    var canvasHeight = canvas.height;
    var isDragging = false;

    function handleMouseDown(e) {
        canMouseX = parseInt(e.clientX - offsetX);
        canMouseY = parseInt(e.clientY - offsetY);
        // set the drag flag
        isDragging = true;
    }

    function handleMouseUp(e) {
        canMouseX = parseInt(e.clientX - offsetX);
        canMouseY = parseInt(e.clientY - offsetY);
        // clear the drag flag
        isDragging = false;
    }

    function handleMouseOut(e) {
        canMouseX = parseInt(e.clientX - offsetX);
        canMouseY = parseInt(e.clientY - offsetY);
        // user has left the canvas, so clear the drag flag
        //isDragging=false;
    }

    function handleMouseMove(e) {
        canMouseX = parseInt(e.clientX - offsetX);
        canMouseY = parseInt(e.clientY - offsetY);
        // if the drag flag is set, clear the canvas and draw the image
        if (isDragging) {
            //ctx.clearRect(0, 0, canvasWidth, canvasHeight);
            //ctx.drawImage(img, canMouseX - 128 / 2, canMouseY - 120 / 2, 128, 120);
            containers[si].x = canMouseX - 128 / 2;
            containers[si].y = canMouseY - 120 / 2;
            redraw();
        }
    }


    $("#contextCanvas").mousedown(function(e) { handleMouseDown(e); });
    $("#contextCanvas").mousemove(function(e) { handleMouseMove(e); });
    $("#contextCanvas").mouseup(function(e) { handleMouseUp(e); });
    $("#contextCanvas").mouseout(function(e) { handleMouseOut(e); });


    $('#slider_d0x').val(containers[si].d0x);
    $('#slider_d0x').on("change mousemove", function() {
        containers[si].d0x = parseFloat($(this).val());
        redraw();
    });
    $('#slider_d0y').val(containers[si].d0y);
    $('#slider_d0y').on("change mousemove", function() {
        containers[si].d0y = parseFloat($(this).val());
        redraw();
    });

    $('#slider_d1x').val(containers[si].d1x);
    $('#slider_d1x').on("change mousemove", function() {
        containers[si].d1x = parseFloat($(this).val());
        redraw();
    });
    $('#slider_d1y').val(containers[si].d1y);
    $('#slider_d1y').on("change mousemove", function() {
        containers[si].d1y = parseFloat($(this).val());
        redraw();
    });
    $('#slider_d2x').val(containers[si].d2x);
    $('#slider_d2x').on("change mousemove", function() {
        containers[si].d2x = parseFloat($(this).val());
        redraw();
    });
    $('#slider_d2y').val(containers[si].d2y);
    $('#slider_d2y').on("change mousemove", function() {
        containers[si].d2y = parseFloat($(this).val());
        redraw();
    });
    $('#slider_d3x').val(containers[si].d3x);
    $('#slider_d3x').on("change mousemove", function() {
        containers[si].d3x = parseFloat($(this).val());
        redraw();
    });
    $('#slider_d3y').val(containers[si].d3y);
    $('#slider_d3y').on("change mousemove", function() {
        containers[si].d3y = parseFloat($(this).val());
        redraw();
    });


    $('#container_width').val(containers[si].width);
    $('#container_width').on("change", function() {
        containers[si].width = parseFloat($(this).val());
        redraw();
    });
    $('#container_height').val(containers[si].height);
    $('#container_height').on("change", function() {
        containers[si].height = parseFloat($(this).val());
        redraw();
    });




    function replicateImage(repeats, image, hotspot, repeatHorizontally) {
        var canvas = document.createElement('canvas');
        canvas.height = hotspot.height;
        canvas.width = hotspot.width;
        var context = canvas.getContext("2d");

        var aspectRatio = (image.width / image.height);
        var minPadding = 0; //5 right and 5 left
        var padding = 0;

        if (repeatHorizontally) {
            var imgWidth = aspectRatio * hotspot.height;
            var widthOfImages = repeats * (imgWidth + minPadding * 2);
            padding = (hotspot.width - widthOfImages) / (repeats * 2) + minPadding;

            for (var i = 0; i < repeats; i++) {
                var x = (imgWidth * i) + (padding * (i * 2) + padding);
                context.drawImage(image, x, 0, imgWidth, canvas.height);
            }
        } else {
            var imgHeight = hotspot.width / aspectRatio;
            var heightOfImages = repeats * (imgHeight + minPadding * 2);
            padding = (hotspot.height - heightOfImages) / (repeats * 2) + minPadding;

            for (var i = 0; i < repeats; i++) {
                var y = (imgHeight * i) + (padding * (i * 2) + padding);
                context.drawImage(image, 0, y, hotspot.width, imgHeight);
            }
        }

        return canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
    }

    function resizeImage(url, thumbHeight, callback) {
        var screenShot = new Image();
        screenShot.src = url;

        var cnv = document.createElement('canvas');
        var ctx = cnv.getContext("2d");

        screenShot.onload = function() {
            newHeight = Math.floor(screenShot.height * (0.61));

            if (newHeight < thumbHeight) {
                newHeight = thumbHeight;
            }

            newWidth = Math.floor(screenShot.width / screenShot.height * newHeight);

            if (newHeight >= thumbHeight) {
                cnv.width = newWidth;
                cnv.height = newHeight;

                ctx.drawImage(screenShot, 0, 0, newWidth, newHeight);

                screenShot.src = cnv.toDataURL();
                screenShot.height = newHeight;
            }

            callback(screenShot);
        };
    }

    function createBackgroundImage(hotspot) {

        var canvas = document.createElement('canvas');
        canvas.height = hotspot.height;
        canvas.width = hotspot.width;
        var context = canvas.getContext("2d");

        context.fillStyle = hotspot.bgColor;
        context.fillRect(0, 0, canvas.width, canvas.height);

        return canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
    }
})();