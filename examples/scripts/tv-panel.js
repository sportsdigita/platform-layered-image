var data = {
    backgroundImageUrl: 'images/tv-panel.jpg',
    logos: [
        // { "src": "images/packerhelmet.png" },
        // { "src": "images/Red_Sox.jpg" },
        // { "src": "images/iTunes.png" },
        // { "src": "images/ducks.png" },
        { "src": "images/Red_Sox_Logo.png" },
        // { "src": "images/cokeredcircle.jpg" },
        // { "src": "images/CocaCola.jpg" },
        { "src": "images/CocaCola.jpg", "hotspotOrientationId": "2" }
    ],
    hotspots: [{
            "bgColor": "#ffffff",
            "alpha": 1,
            "padding": 20,
            "d0x": 604,
            "d0y": 268,
            "d1x": 926,
            "d1y": 219,
            "d2x": 930,
            "d2y": 484,
            "d3x": 608,
            "d3y": 465
        },
        {
            "src": 'images/tv-lines.png',
            "x": 607,
            "y": 249,
            "zIndex": 2,
            "width": 323,
            "height": 228
        }
    ]
};

var layeredImage = new PlatformLayeredImage({
    backgroundImageUrl: data.backgroundImageUrl,
    logos: data.logos,
    hotspots: data.hotspots,
    onRendered: function(canvas, context) {
        var imageData = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
        $('.canvasImageContainer').css('background-image', 'url(' + imageData + ')');
    }
});