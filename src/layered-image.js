var LayeredImage = function(options) {
    var self = this;
    var backgroundImage = options.backgroundImage;
    var layers;

    var canvas = (options.canvasId) ? document.getElementById(options.canvasId) : document.createElement('canvas'),
        context = canvas.getContext('2d'),
        img1 = new Image(),
        imageLoaded = false,
        originalImages = [],
        scale = options.scale || 1,
        layerImages = [];

    img1.crossOrigin = "anonymous";
    canvas.width = canvas.height = 1024;

    self.getCanvas = function() {
        return canvas;
    };

    self.getContext = function() {
        return context;
    };

    self.getLayers = function() {
        return layers;
    };

    self.getScale = function() {
        return scale;
    };

    self.setScale = function(newScale) {
        scale = newScale;
        resizeCanvas();
    };

    self.addLayer = function(layer) {
        if (!layers) {
            layers = [];
        }

        layers.push(layer);
        self.reinitializeLayers();
    };

    self.deleteLayer = function(layer) {
        if (!layers) {
            layers = [];
        }

        if (layers.indexOf(layer) != -1) {
            layers.splice(layers.indexOf(layer), 1);
            self.reinitializeLayers();
        }
    };

    self.setLayers = function(newLayers) {
        layers = newLayers;
        initializeLayers();
    };

    self.hideLayer = function(layer) {
        layer.hide = true;
        self.redraw(true);
    };

    self.showLayer = function(layer) {
        layer.hide = false;
        self.redraw(true);
    };

    self.showAllLayers = function() {
        $.each(layers, function(i, layer) { layer.hide = false; });
        self.redraw(true);
    };

    self.hideAllLayers = function() {
        $.each(layers, function(i, layer) { layer.hide = true; });
        self.redraw(true);
    };

    self.initialize = function() {
        img1.src = backgroundImage;
        img1.onload = function() {
            imageLoaded = true;
            resizeCanvas();
        };
    };

    self.isSkewedLayer = function(layer) {
        return (typeof layer.d0x != "undefined" && typeof layer.d0y != "undefined" && typeof layer.d1x != "undefined" && typeof layer.d1y != "undefined" && typeof layer.d2x != "undefined" && typeof layer.d2y != "undefined" && typeof layer.d3x != "undefined" && typeof layer.d3y != "undefined");
    };

    self.reinitializeLayers = function() {
        layerImages.length = 0;
        initializeLayers();
    };

    function loadImage(url, callback) {
        if (originalImages[url]) {
            return callback(originalImages[url]);
        }

        var i = new Image();
        i.crossOrigin = "anonymous";
        i.src = url;
        i.onload = function() {
            i.onload = null;

            if (!originalImages[url]) {
                originalImages[url] = i;
            }
            callback(i);
        };
    }

    var initializeLayers = function() {
        if (!layers || !layers.length) {
            resizeCanvas();
            return;
        }

        $.each(layers, function(index, layer) {
            var layerData = { isLoaded: false, data: layer };
            layerImages.push(layerData);
            loadImage(layer.image, function(img) {
                resizeImage(img, getLayerWidthHeight(layer).height, function(resizedImg) {
                    var rimg = new Image();
                    rimg.crossOrigin = "anonymous";
                    rimg.src = replicateImage(resizedImg, layer);
                    rimg.onload = function() {
                        rimg.onload = null;
                        if (layer.bgColor && layer.bgColor.length) {
                            var backgroundImage = new Image();
                            backgroundImage.crossOrigin = "anonymous";
                            backgroundImage.src = createBackgroundImage(layer);
                            backgroundImage.onload = function() {
                                backgroundImage.onload = null;
                                layerData.img = rimg;
                                layerData.backgroundImage = backgroundImage;
                                layerData.isLoaded = true;
                                resizeCanvas();
                            };
                        } else {
                            layerData.img = rimg;
                            layerData.isLoaded = true;
                            resizeCanvas();
                        }
                    };
                });
            });

        });
    };

    self.redraw = function(useCache) {
        var allLoaded = imageLoaded;
        var sortedLayers = layerImages.slice();

        //clear canvas so we can clear the red box drawing if the main image has transparency.
        context.clearRect(0, 0, canvas.width, canvas.height);

        if (imageLoaded) {
            context.save();
            context.drawImage(img1, 0, 0);
            context.restore();
        }

        if (sortedLayers && sortedLayers.length) {
            sortedLayers.sort(function(a, b) { return a.data.zIndex - b.data.zIndex; });
        }

        $.each(sortedLayers, function(index, layerImage) {
            if(layerImage.data.hide){
                return;
            }
            
            if (layerImage.isLoaded) {
                if (layerImage.backgroundImage) {
                    renderLayer(layerImage.backgroundImage, layerImage.data, useCache, "background");
                }
                renderLayer(layerImage.img, layerImage.data, useCache, "forground");
            } else {
                allLoaded = false;
            }
        });

        if (options.onRendered && allLoaded)
            options.onRendered(canvas, context);
    };

    var renderLayer = function(img, data, useCache, cacheName) {
        if (typeof data.alpha != "undefined") {
            context.globalAlpha = data.alpha;
        }

        if (typeof data.d0x != "undefined" && typeof data.d0y != "undefined" && typeof data.d1x != "undefined" && typeof data.d1y != "undefined" && typeof data.d2x != "undefined" && typeof data.d2y != "undefined" && typeof data.d3x != "undefined" && typeof data.d3y != "undefined") {
            if (data[cacheName] && useCache) {
                var minX = Math.min(data.d0x, data.d1x, data.d2x, data.d3x);
                var minY = Math.min(data.d0y, data.d1y, data.d2y, data.d3y);
                context.save();
                context.drawImage(data[cacheName].img, minX, minY);
                context.restore();
                // console.log("using cache - x:" + minX + " y:" + minY);
            } else {
                var image = new Perspective().draw(img, context, data);
                data[cacheName] = image;
            }

        } else if (typeof data.x != "undefined" && typeof data.y != "undefined" && data.width && data.height) {
            context.save();
            context.drawImage(img, data.x, data.y, data.width, data.height);
            context.restore();
        }

        context.globalAlpha = 1;
        context.restore();
    };

    var resizeCanvas = function() {
        if (!img1 || !canvas || !context)
            return;

        canvas.width = img1.width * scale;
        canvas.height = img1.height * scale;

        context.restore();
        // context.setTransform(scale, 0, 0, scale, 0, 0);
        context.scale(scale, scale);
        context.save();

        self.redraw();
    };

    var replicateImage = function(image, hotspot) {

        var canvas = document.createElement('canvas');
        var size = getLayerWidthHeight(hotspot);
        canvas.height = size.height;
        canvas.width = size.width;
        var context = canvas.getContext("2d");


        var aspectRatio = (image.width / image.height);
        var hsRatio = size.width / size.height;
        var repeatHorizontally = (aspectRatio < hsRatio);

        var minPadding = hotspot.padding || 0; //5 right and 5 left
        var padding = hotspot.padding || 0;
        var repeats = 1;

        if (repeatHorizontally) {
            var imgWidth = aspectRatio * size.height;

            if (hotspot.replicate) {
                repeats = Math.floor(size.width / imgWidth);
                if (typeof hotspot.maxRepeatHorizontal != "undefined" && repeats > hotspot.maxRepeatHorizontal)
                    repeats = hotspot.maxRepeatHorizontal;

            }

            var widthOfImages = repeats * (imgWidth + minPadding * 2);
            var imgX = (size.width - widthOfImages) / (repeats * 2) + minPadding;

            for (var i = 0; i < repeats; i++) {
                var x = ((imgWidth * i) + (imgX * (i * 2) + imgX)) + padding;
                var y = 0 + padding;
                var width = imgWidth - (padding * 2);
                var heigth = canvas.height - (padding * 2);
                context.drawImage(image, x, y, width, heigth);
            }
        } else {
            var imgHeight = size.width / aspectRatio;
            if (hotspot.replicate) {
                repeats = Math.floor(size.height / imgHeight);
                if (typeof hotspot.maxRepeatVertical != "undefined" && repeats > hotspot.maxRepeatVertical)
                    repeats = hotspot.maxRepeatVertical;
            }
            var heightOfImages = repeats * (imgHeight + minPadding * 2);
            var imgY = (size.height - heightOfImages) / (repeats * 2) + minPadding;


            for (var z = 0; z < repeats; z++) {
                var x = 0 + padding;
                var y = (imgHeight * z) + (imgY * (z * 2) + imgY) + padding;
                var width = size.width - (padding * 2);
                var height = imgHeight - (padding * 2);
                context.drawImage(image, x, y, width, height);
            }
        }

        return canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
    };

    var getLayerWidthHeight = function(data) {
        var size = {};
        if (typeof data.d0x != "undefined" && typeof data.d0y != "undefined" && typeof data.d1x != "undefined" && typeof data.d1y != "undefined" && typeof data.d2x != "undefined" && typeof data.d2y != "undefined" && typeof data.d3x != "undefined" && typeof data.d3y != "undefined") {
            var topWidth = Math.floor(Math.sqrt(((data.d0x - data.d1x) * (data.d0x - data.d1x)) + ((data.d0y - data.d1y) * (data.d0y - data.d1y))));
            var bottomWidth = Math.floor(Math.sqrt(((data.d3x - data.d2x) * (data.d3x - data.d2x)) + ((data.d3y - data.d2y) * (data.d3y - data.d2y))));
            var leftHeight = Math.floor(Math.sqrt(((data.d0x - data.d3x) * (data.d0x - data.d3x)) + ((data.d0y - data.d3y) * (data.d0y - data.d3y))));
            var rightHeight = Math.floor(Math.sqrt(((data.d1x - data.d2x) * (data.d1x - data.d2x)) + ((data.d1y - data.d2y) * (data.d1y - data.d2y))));

            size.width = Math.max(topWidth, bottomWidth);
            size.height = Math.max(leftHeight, rightHeight);
        } else if (data.width && data.height) {
            size.width = data.width;
            size.height = data.height;
        }
        return size;
    };

    var resizeImage = function(img, thumbHeight, callback) {
        var c = document.createElement("canvas");
        var context = canvas.getContext("2d");
        context.webkitImageSmoothingEnabled = false;
        context.mozImageSmoothingEnabled = false;
        context.imageSmoothingEnabled = false; /// future

        var scale = thumbHeight / img.height;
        if (scale < 1) {
            var dc = downScaleImage(img, scale);
            var resizeImage = new Image();
            resizeImage.crossOrigin = "anonymous";
            resizeImage.onload = function() {
                resizeImage.onload = null;
                callback(resizeImage);
            };
            resizeImage.src = dc.toDataURL();
        } else {
            callback(img);
        }
    };

    var createBackgroundImage = function(hotspot) {

        var canvas = document.createElement('canvas');
        var size = getLayerWidthHeight(hotspot);
        canvas.height = size.height;
        canvas.width = size.width;
        var context = canvas.getContext("2d");

        context.fillStyle = hotspot.bgColor;
        context.fillRect(0, 0, size.width, size.height);

        return canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
    };




    // scales the image by (float) scale < 1
    // returns a canvas containing the scaled image.
    var downScaleImage = function(img, scale) {
        var imgCV = document.createElement('canvas');
        imgCV.width = img.width;
        imgCV.height = img.height;
        var imgCtx = imgCV.getContext('2d');
        imgCtx.drawImage(img, 0, 0);
        return downScaleCanvas(imgCV, scale);
    };

    // scales the canvas by (float) scale < 1
    // returns a new canvas containing the scaled image.
    var downScaleCanvas = function(cv, scale) {
        if (!(scale < 1) || !(scale > 0)) throw ('scale must be a positive number <1 ');
        var sqScale = scale * scale; // square scale = area of source pixel within target
        var sw = cv.width; // source image width
        var sh = cv.height; // source image height
        var tw = Math.floor(sw * scale); // target image width
        var th = Math.floor(sh * scale); // target image height
        var sx = 0,
            sy = 0,
            sIndex = 0; // source x,y, index within source array
        var tx = 0,
            ty = 0,
            yIndex = 0,
            tIndex = 0; // target x,y, x,y index within target array
        var tX = 0,
            tY = 0; // rounded tx, ty
        var w = 0,
            nw = 0,
            wx = 0,
            nwx = 0,
            wy = 0,
            nwy = 0; // weight / next weight x / y
        // weight is weight of current source point within target.
        // next weight is weight of current source point within next target's point.
        var crossX = false; // does scaled px cross its current px right border ?
        var crossY = false; // does scaled px cross its current px bottom border ?
        var sBuffer = cv.getContext('2d').
        getImageData(0, 0, sw, sh).data; // source buffer 8 bit rgba
        var tBuffer = new Float32Array(4 * tw * th); // target buffer Float32 rgb
        var sR = 0,
            sG = 0,
            sB = 0,
            sA = 0; //source alpha 

        for (sy = 0; sy < sh; sy++) {
            ty = sy * scale; // y src position within target
            tY = 0 | ty; // rounded : target pixel's y
            yIndex = 4 * tY * tw; // line index within target array
            crossY = (tY != (0 | ty + scale));
            if (crossY) { // if pixel is crossing botton target pixel
                wy = (tY + 1 - ty); // weight of point within target pixel
                nwy = (ty + scale - tY - 1); // ... within y+1 target pixel
            }
            for (sx = 0; sx < sw; sx++, sIndex += 4) {
                tx = sx * scale; // x src position within target
                tX = 0 | tx; // rounded : target pixel's x
                tIndex = yIndex + tX * 4; // target pixel index within target array
                crossX = (tX != (0 | tx + scale));
                if (crossX) { // if pixel is crossing target pixel's right
                    wx = (tX + 1 - tx); // weight of point within target pixel
                    nwx = (tx + scale - tX - 1); // ... within x+1 target pixel
                }
                sR = sBuffer[sIndex]; // retrieving r,g,b for curr src px.
                sG = sBuffer[sIndex + 1];
                sB = sBuffer[sIndex + 2];
                sA = sBuffer[sIndex + 3];

                if (!crossX && !crossY) { // pixel does not cross
                    // just add components weighted by squared scale.
                    tBuffer[tIndex] += sR * sqScale;
                    tBuffer[tIndex + 1] += sG * sqScale;
                    tBuffer[tIndex + 2] += sB * sqScale;
                    tBuffer[tIndex + 3] += sA * sqScale;
                } else if (crossX && !crossY) { // cross on X only
                    w = wx * scale;
                    // add weighted component for current px
                    tBuffer[tIndex] += sR * w;
                    tBuffer[tIndex + 1] += sG * w;
                    tBuffer[tIndex + 2] += sB * w;
                    tBuffer[tIndex + 3] += sA * w;
                    // add weighted component for next (tX+1) px                
                    nw = nwx * scale
                    tBuffer[tIndex + 4] += sR * nw;
                    tBuffer[tIndex + 5] += sG * nw;
                    tBuffer[tIndex + 6] += sB * nw;
                    tBuffer[tIndex + 7] += sA * nw;
                } else if (crossY && !crossX) { // cross on Y only
                    w = wy * scale;
                    // add weighted component for current px
                    tBuffer[tIndex] += sR * w;
                    tBuffer[tIndex + 1] += sG * w;
                    tBuffer[tIndex + 2] += sB * w;
                    tBuffer[tIndex + 3] += sA * w;
                    // add weighted component for next (tY+1) px                
                    nw = nwy * scale
                    tBuffer[tIndex + 4 * tw] += sR * nw;
                    tBuffer[tIndex + 4 * tw + 1] += sG * nw;
                    tBuffer[tIndex + 4 * tw + 2] += sB * nw;
                    tBuffer[tIndex + 4 * tw + 3] += sA * nw;
                } else { // crosses both x and y : four target points involved
                    // add weighted component for current px
                    w = wx * wy;
                    tBuffer[tIndex] += sR * w;
                    tBuffer[tIndex + 1] += sG * w;
                    tBuffer[tIndex + 2] += sB * w;
                    tBuffer[tIndex + 3] += sA * w;
                    // for tX + 1; tY px
                    nw = nwx * wy;
                    tBuffer[tIndex + 4] += sR * nw;
                    tBuffer[tIndex + 5] += sG * nw;
                    tBuffer[tIndex + 6] += sB * nw;
                    tBuffer[tIndex + 7] += sA * nw;
                    // for tX ; tY + 1 px
                    nw = wx * nwy;
                    tBuffer[tIndex + 4 * tw] += sR * nw;
                    tBuffer[tIndex + 4 * tw + 1] += sG * nw;
                    tBuffer[tIndex + 4 * tw + 2] += sB * nw;
                    tBuffer[tIndex + 4 * tw + 3] += sA * nw;
                    // for tX + 1 ; tY +1 px
                    nw = nwx * nwy;
                    tBuffer[tIndex + 4 * tw + 4] += sR * nw;
                    tBuffer[tIndex + 4 * tw + 5] += sG * nw;
                    tBuffer[tIndex + 4 * tw + 6] += sB * nw;
                    tBuffer[tIndex + 4 * tw + 7] += sA * nw;
                }
            } // end for sx 
        } // end for sy

        // create result canvas
        var resCV = document.createElement('canvas');
        resCV.width = tw;
        resCV.height = th;
        var resCtx = resCV.getContext('2d');
        var imgRes = resCtx.getImageData(0, 0, tw, th);
        var tByteBuffer = imgRes.data;
        // convert float32 array into a UInt8Clamped Array
        var pxIndex = 0; //  
        for (sIndex = 0, tIndex = 0; pxIndex < tw * th; sIndex += 4, tIndex += 4, pxIndex++) {
            tByteBuffer[tIndex] = Math.ceil(tBuffer[sIndex]);
            tByteBuffer[tIndex + 1] = Math.ceil(tBuffer[sIndex + 1]);
            tByteBuffer[tIndex + 2] = Math.ceil(tBuffer[sIndex + 2]);
            tByteBuffer[tIndex + 3] = Math.ceil(tBuffer[sIndex + 3]);
            // tByteBuffer[tIndex + 3] = 255;
        }
        // writing result to canvas.
        resCtx.putImageData(imgRes, 0, 0);
        return resCV;
    };

    self.convertFromSkew = function(layer) {
        if (!self.isSkewedLayer(layer)) {
            return;
        }

        var maxX = Math.max.apply(null, [layer.d0x, layer.d1x, layer.d2x, layer.d3x]);
        var minX = Math.min.apply(null, [layer.d0x, layer.d1x, layer.d2x, layer.d3x]);
        var maxY = Math.max.apply(null, [layer.d0y, layer.d1y, layer.d2y, layer.d3y]);
        var minY = Math.min.apply(null, [layer.d0y, layer.d1y, layer.d2y, layer.d3y]);

        layer.x = minX;
        layer.y = minY;
        layer.height = maxY - minY;
        layer.width = maxX - minX;

        delete layer.d0x;
        delete layer.d0y;
        delete layer.d1x;
        delete layer.d1y;
        delete layer.d2x;
        delete layer.d2y;
        delete layer.d3x;
        delete layer.d3y;

        return layer;
    };

    self.convertToSkew = function(layer) {
        if (self.isSkewedLayer(layer)) {
            return;
        }

        layer.d0x = layer.x;
        layer.d0y = layer.y;
        layer.d1x = layer.x + layer.width;
        layer.d1y = layer.y;
        layer.d2x = layer.x + layer.width;
        layer.d2y = layer.y + layer.height;
        layer.d3x = layer.x;
        layer.d3y = layer.y + layer.height;

        delete layer.x;
        delete layer.y;
        delete layer.height;
        delete layer.width;

        return layer;
    };

    self.destroy = function() {
        canvas = null;
        context = null;
        img1 = null;
        imageLoaded = false;
        originalImages = null;
        layerImages = null;
    };

    self.initialize();
};