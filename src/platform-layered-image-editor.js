var PlatformLayeredImageEditor = function(options) {
    //inherit from LayeredImage
    PlatformLayeredImage.call(this, options);
    var parent = $.extend({}, this);

    //defaults
    var defaultOptions = {
        outlinePadding: 10,
        outlineColor: "#ff0000",
        outlineWidth: 3,
        transparentOutlinePadding: -1,
        transparentOutlineColor: "#ff0000",
        transparentOutlineWidth: 1,
    };

    $.extend(options, defaultOptions);

    var self = this;
    var offset = {
        x: 0,
        y: 0
    };
    var selectedHotspot;
    var resizeableImage;
    var resizingImage = false;

    self.getSelectedHotspot = function() {
        return selectedHotspot;
    };

    self.setSelectedHotspot = function(hotspot) {
        selectedHotspot = hotspot;
        self.redraw(true);
    };

    self.hideHotspot = function(hotspot) {
        if (hotspot && selectedHotspot == hotspot) {
            selectedHotspot = null;
            if (options.onHotspotDeselected)
                options.onHotspotDeselected();
        }
        parent.hideHotspot(hotspot);
        if (selectedHotspot)
            outline(selectedHotspot);
    };

    self.showHotspot = function(hotspot) {
        parent.showHotspot(hotspot);
        if (selectedHotspot)
            outline(selectedHotspot);
    };

    self.showAllHotspots = function() {
        parent.showAllHotspots();
        if (selectedHotspot)
            outline(selectedHotspot);
    };

    self.hideAllHotspots = function() {
        var hotspotSelected = selectedHotspot != null;
        selectedHotspot = null;
        if (hotspotSelected && options.onHotspotDeselected)
            options.onHotspotDeselected();

        parent.hideAllHotspots();
        if (selectedHotspot)
            outline(selectedHotspot);
    };

    self.deleteHotspot = function(hotspot) {
        if (hotspot && selectedHotspot == hotspot) {
            selectedHotspot = null;
            if (options.onHotspotDeselected)
                options.onHotspotDeselected();
        }
        parent.deleteHotspot(hotspot);
    };

    self.setScale = function(newScale) {
        parent.setScale(newScale);

        if (resizeableImage) {
            resizeableImage.setScale(newScale);
        }
    };

    self.redraw = function(useCache) {
        parent.redraw(useCache);

        if (selectedHotspot)
            outline(selectedHotspot);
    };

    function getClickLocation(event, offset) {
        var topOffset = offset.top || 0;
        var leftOffset = offset.left || 0;

        return {
            x: (event.pageX - leftOffset) / self.getScale(),
            y: (event.pageY - topOffset) / self.getScale()
        };
    }

    function getHotspotCoordinates(hotspot, padding) {

        if (typeof padding === "undefined")
            padding = 0;

        if (self.isSkewedLayer(hotspot)) {
            return [{
                    x: hotspot.d0x - padding,
                    y: hotspot.d0y - padding
                },
                {
                    x: hotspot.d1x + padding,
                    y: hotspot.d1y - padding
                },
                {
                    x: hotspot.d2x + padding,
                    y: hotspot.d2y + padding
                },
                {
                    x: hotspot.d3x - padding,
                    y: hotspot.d3y + padding
                }
            ];
        } else if (typeof hotspot.x != "undefined" && typeof hotspot.y != "undefined" && hotspot.width && hotspot.height) {
            return [{
                    x: hotspot.x - padding,
                    y: hotspot.y - padding
                },
                {
                    x: hotspot.x + hotspot.width + padding,
                    y: hotspot.y - padding
                },
                {
                    x: hotspot.x + hotspot.width + padding,
                    y: hotspot.y + hotspot.height + padding
                },
                {
                    x: hotspot.x - padding,
                    y: hotspot.y + hotspot.height + padding
                }
            ];
        }

    }

    function calculateMoveDistance(clickLocation, newLocation) {
        return {
            x: newLocation.x - clickLocation.x,
            y: newLocation.y - clickLocation.y
        };
    }

    function moveHotspot(moveDistance, hotspot) {
        if (typeof hotspot.d0x != "undefined")
            hotspot.d0x += moveDistance.x;
        if (typeof hotspot.d0y != "undefined")
            hotspot.d0y += moveDistance.y;

        if (typeof hotspot.d1x != "undefined")
            hotspot.d1x += moveDistance.x;
        if (typeof hotspot.d1y != "undefined")
            hotspot.d1y += moveDistance.y;

        if (typeof hotspot.d2x != "undefined")
            hotspot.d2x += moveDistance.x;
        if (typeof hotspot.d2y != "undefined")
            hotspot.d2y += moveDistance.y;

        if (typeof hotspot.d3x != "undefined")
            hotspot.d3x += moveDistance.x;
        if (typeof hotspot.d3y != "undefined")
            hotspot.d3y += moveDistance.y;

        if (typeof hotspot.x != "undefined")
            hotspot.x += moveDistance.x;
        if (typeof hotspot.y != "undefined")
            hotspot.y += moveDistance.y;
    }

    function outline(hotspot) {
        var coordinates = getHotspotCoordinates(hotspot, options.outlinePadding);

        var startingPoint = null;
        var ctx = self.getContext();
        ctx.lineWidth = options.outlineWidth;
        ctx.beginPath();

        $.each(coordinates, function(i, coordinate) {
            if (i > 0) {
                ctx.lineTo(coordinate.x, coordinate.y);
            } else {
                startingPoint = coordinate;
                ctx.moveTo(coordinate.x, coordinate.y);
            }
        });
        ctx.lineTo(startingPoint.x, startingPoint.y);
        ctx.strokeStyle = options.outlineColor;
        ctx.stroke();

        if (!hotspot.bgColor && self.isSkewedLayer(hotspot) && !hotspot.image) {
            coordinates = getHotspotCoordinates(hotspot, options.transparentOutlinePadding);

            ctx.lineWidth = options.transparentOutlineWidth;
            ctx.beginPath();

            $.each(coordinates, function(i, coordinate) {
                if (i > 0) {
                    ctx.lineTo(coordinate.x, coordinate.y);
                } else {
                    startingPoint = coordinate;
                    ctx.moveTo(coordinate.x, coordinate.y);
                }
            });
            ctx.lineTo(startingPoint.x, startingPoint.y);
            ctx.strokeStyle = options.transparentOutlineColor;
            ctx.stroke();
        }

        self.updateLayers();


        if (!resizeableImage) {
            resizeableImage = new ResizeableImage({
                layer: hotspot,
                canvasId: options.canvasId,
                scale: self.getScale(),
                onDragging: function() {
                    resizingImage = true;
                    self.redraw(false);
                },
                onDraggingStopped: function() {
                    self.reinitializeLayers();
                    resizingImage = false;

                    if (options.onHotspotUpdated)
                        options.onHotspotUpdated(selectedHotspot);
                }
            });
        } else {
            resizeableImage.setLayer(hotspot);
        }

        resizeableImage.redraw();
    }

    function inside(pt, poly) {
        for (var c = false, i = -1, l = poly.length, j = l - 1; ++i < l; j = i)
            ((poly[i].y <= pt.y && pt.y < poly[j].y) || (poly[j].y <= pt.y && pt.y < poly[i].y)) &&
            (pt.x < (poly[j].x - poly[i].x) * (pt.y - poly[i].y) / (poly[j].y - poly[i].y) + poly[i].x) &&
            (c = !c);
        return c;
    }

    self.nudgeRight = function(hotspot) {
        if (self.isSkewedLayer(hotspot)) {
            hotspot.d0x += 1;
            hotspot.d1x += 1;
            hotspot.d2x += 1;
            hotspot.d3x += 1;
        } else if (typeof hotspot.x != "undefined" && typeof hotspot.y != "undefined") {
            hotspot.x += 1;
        }

        self.updateLayers();
        self.redraw(true);

        if (options.onHotspotUpdated)
            options.onHotspotUpdated(hotspot);
    };

    self.nudgeLeft = function(hotspot) {
        if (self.isSkewedLayer(hotspot)) {
            hotspot.d0x -= 1;
            hotspot.d1x -= 1;
            hotspot.d2x -= 1;
            hotspot.d3x -= 1;
        } else if (typeof hotspot.x != "undefined" && typeof hotspot.y != "undefined") {
            hotspot.x -= 1;
        }

        self.updateLayers();
        self.redraw(true);

        if (options.onHotspotUpdated)
            options.onHotspotUpdated(hotspot);
    };

    self.nudgeDown = function(hotspot) {
        if (self.isSkewedLayer(hotspot)) {
            hotspot.d0y += 1;
            hotspot.d1y += 1;
            hotspot.d2y += 1;
            hotspot.d3y += 1;
        } else if (typeof hotspot.x != "undefined" && typeof hotspot.y != "undefined") {
            hotspot.y += 1;
        }

        self.updateLayers();
        self.redraw(true);

        if (options.onHotspotUpdated)
            options.onHotspotUpdated(hotspot);
    };

    self.nudgeUp = function(hotspot) {
        if (self.isSkewedLayer(hotspot)) {
            hotspot.d0y -= 1;
            hotspot.d1y -= 1;
            hotspot.d2y -= 1;
            hotspot.d3y -= 1;
        } else if (typeof hotspot.x != "undefined" && typeof hotspot.y != "undefined") {
            hotspot.y -= 1;
        }

        self.updateLayers();
        self.redraw(true);

        if (options.onHotspotUpdated)
            options.onHotspotUpdated(hotspot);
    };

    var draggingHotspot = false,
        clickLocation;

    $(self.getCanvas()).mousedown(function(e) {

            clickLocation = getClickLocation(e, $(this).offset());

            if (resizeableImage && resizeableImage.hitAnchor(clickLocation.x, clickLocation.y))
                return;

            selectedHotspot = null;

            $.each(self.getHotspots(), function(i, hotspot) {
                var coordinates = getHotspotCoordinates(hotspot);

                if (inside(clickLocation, coordinates) && !hotspot.hide) {
                    selectedHotspot = hotspot;
                }
            });

            if (!selectedHotspot) {
                self.redraw(true);

                if (options.onHotspotDeselected)
                    options.onHotspotDeselected();

                return;
            }

            self.redraw(true);

            draggingHotspot = true;

            if (options.onHotspotSelected)
                options.onHotspotSelected(selectedHotspot);
        })
        .mousemove(function(e) {
            if (!draggingHotspot || resizingImage || (resizeableImage && resizeableImage.isDraggingAnchor()))
                return;

            var moveDistance = calculateMoveDistance(clickLocation, getClickLocation(e, $(this).offset()));
            moveHotspot(moveDistance, selectedHotspot);
            clickLocation.x += moveDistance.x;
            clickLocation.y += moveDistance.y;

            self.updateLayers();
            self.redraw(true);
        })
        .mouseup(function() {
            if (options.onHotspotUpdated && draggingHotspot)
                options.onHotspotUpdated(selectedHotspot);
            draggingHotspot = false;
        })
        .mouseout(function() {
            draggingHotspot = false;
        });

    $(document).keydown(function(event) {
        console.log(event.keyCode);
        switch (event.keyCode) {
            //right arrow
            case 39:
                if (selectedHotspot) {
                    self.nudgeRight(selectedHotspot);
                }
                break;
                //left arrow
            case 37:
                if (selectedHotspot) {
                    self.nudgeLeft(selectedHotspot);
                }
                break;
                //down arrow
            case 40:
                if (selectedHotspot) {
                    self.nudgeDown(selectedHotspot);
                }
                break;
                //up arrow
            case 38:
                if (selectedHotspot) {
                    self.nudgeUp(selectedHotspot);
                }
                break;
        }
    });


    self.destroy = function() {
        if (resizeableImage) {
            resizeableImage.destroy();
        }

        $(self.getCanvas()).off('mousedown');
        $(self.getCanvas()).off('mousemove');
        $(self.getCanvas()).off('mouseup');
        $(self.getCanvas()).off('mouseout');
        $(document).off('keydown');

        var selectedHotspot = null;
        var resizeableImage = null;

        parent.destroy();

    };
};