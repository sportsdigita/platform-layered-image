var PlatformLayeredImage = function(options) {
    //inherit from LayeredImage
    LayeredImage.call(this, options);
    var parent = $.extend({}, this);
    var self = this;
    var logos = options.logos || null;
    var hotspots = options.hotspots || null;

    var addIdsToHotspots = function(hotspots) {
        if (hotspots && hotspots.length) {
            hotspots.forEach(function(hotspot) {
                if (hotspot && !hotspot.id) {
                    hotspot.id = Date.now() + Math.random().toString(36).substring(2, 15);
                };
            })
        }
    }
    addIdsToHotspots(hotspots);

    self.getLogos = function() {
        return logos;
    };

    self.setLogos = function(newLogos) {
        logos = newLogos;
        createLayers();
    };

    self.getHotspots = function() {
        return hotspots;
    };

    self.setHotspots = function(newHotspots) {
        hotspots = newHotspots;
        addIdsToHotspots(hotspots);
        createLayers();
    };

    self.addHotspot = function(hotspot) {
        if (!hotspots) {
            hotspots = [];
        }

        hotspots.push(hotspot);
        addIdsToHotspots(hotspots);
        var layer = createLayer(hotspot);
        self.addLayer(layer);
    };

    self.deleteHotspot = function(hotspot) {
        if (!hotspots) {
            hotspots = [];
        }

        var index = hotspots.indexOf(hotspot);

        if (index != -1) {
            hotspots.splice(index, 1);

            var layer = self.getLayers()[index];

            if (layer) {
                self.deleteLayer(layer);
            }
        }
    };

    self.hideHotspot = function(layer) {
        $.each(hotspots, function(i, hotspot) {
            if (hotspot == layer) {
                hotspot.hide = true;
            }
        });
        self.updateLayers();
        self.hideLayer(layer);
    };

    self.showHotspot = function(layer) {
        $.each(hotspots, function(i, hotspot) {
            if (hotspot == layer) {
                hotspot.hide = false;
            }
        });
        self.updateLayers();
        self.showLayer(layer);
    };

    self.showAllHotspots = function() {
        $.each(hotspots, function(i, hotspot) {
            hotspot.hide = false;
        });
        self.updateLayers();
        self.showAllLayers();
    };

    self.hideAllHotspots = function() {
        $.each(hotspots, function(i, hotspot) {
            hotspot.hide = true;
        });
        self.updateLayers();
        self.hideAllLayers();
    };

    self.updateLayers = function() {
        if (!logos || !hotspots)
            return;

        var layers = self.getLayers();

        $.each(hotspots, function(i, hotspot) {
            var layerIdx = layers ? layers.indexOf(layers.filter(function(layer) { return layer.id == hotspot.id })[0]) : null;
            if(layerIdx == null) {
                return;
            }
            var layer = layers[layerIdx];
            $.each(Object.keys(hotspot), function(i, k) {
                layer[k] = hotspot[k];
            });
        });
    };

    self.hotspotLogoUpdated = function(hotspot) {
        var index = hotspots.indexOf(hotspot);

        if (index == -1)
            return;

        var layer = self.getLayers()[index]

        if (!hotspot.image) {
            var logo, defaultLogo;
            //find correct logo
            $.each(logos, function(index, l) {
                if (!l.hotspotOrientationId || l.hotspotOrientationId.toLowerCase() == "default") {
                    defaultLogo = l;
                } else if (l.hotspotOrientationId && hotspot.preferredOrientation && l.hotspotOrientationId == hotspot.preferredOrientation) {
                    logo = l;
                }
            });

            if (!logo) {
                logo = defaultLogo;
            }

            layer.image = logo.image;
        } else {
            layer.image = hotspot.image;
        }

        self.updateLayers();
        self.reinitializeLayers();
    };

    var createLayer = function(hotspot) {
        var layer;
        if (!hotspot.image) {
            var logo, defaultLogo;
            //find correct logo
            $.each(logos, function(index, l) {
                if (!l.hotspotOrientationId || l.hotspotOrientationId.toLowerCase() == "default") {
                    defaultLogo = l;
                } else if (l.hotspotOrientationId && hotspot.preferredOrientation && l.hotspotOrientationId == hotspot.preferredOrientation) {
                    logo = l;
                }
            });

            if (!logo) {
                logo = defaultLogo;
            }

            layer = $.extend({}, hotspot, logo);
        } else {
            layer = $.extend({}, hotspot);
        }

        return layer;
    };

    var createLayers = function() {
        if (!logos || !hotspots)
            return;
        var layers = [];

        $.each(hotspots, function(i, hotspot) {
            layers.push(createLayer(hotspot));
        });

        self.setLayers(layers);
    };

    self.convertFromSkew = function(hotspot) {
        if (!self.isSkewedLayer(hotspot)) {
            return;
        }

        parent.convertFromSkew(hotspot);

        var index = hotspots.indexOf(hotspot);

        if (index >= 0) {
            var layer = self.getLayers()[index];
            parent.convertFromSkew(layer);
        }

        self.redraw();

        return hotspot;
    };

    self.convertToSkew = function(hotspot) {
        if (self.isSkewedLayer(hotspot)) {
            return;
        }

        parent.convertToSkew(hotspot);

        var index = hotspots.indexOf(hotspot);

        if (index >= 0) {
            var layer = self.getLayers()[index];
            parent.convertToSkew(layer);
        }

        self.redraw();

        return hotspot;
    };

    self.destroy = function() {
        var logos = null;
        var hotspots = null;

        parent.destroy();
    };

    createLayers();
};