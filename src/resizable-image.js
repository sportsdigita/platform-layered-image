var ResizeableImage = function(options) {

    var self = this;

    //defaults 
    var defaultOptions = {
        anchorPadding: 10,
        anchorColor: "#ff0000",
        anchorWidth: 3,
        scale: 1
    };

    options = $.extend(defaultOptions, options);

    var canvas = (options.canvasId) ? document.getElementById(options.canvasId) : document.createElement('canvas'),
        context = canvas.getContext('2d'),
        pi2 = Math.PI * 2,
        layer = options.layer,
        padding = 10,
        anchors = [],
        scale = options.scale,
        resizerRadius = 8;

    var canvasOffset = $(canvas).offset();
    var offsetX = canvasOffset.left;
    var offsetY = canvasOffset.top;

    function drawDragAnchor(x, y) {
        context.beginPath();
        context.fillStyle = options.anchorColor;
        context.arc(x, y, resizerRadius, 0, pi2, false);
        context.closePath();
        context.fill();
        anchors.push({ x: x, y: y, r: resizerRadius });
    }


    self.setLayer = function(newLayer) {
        layer = newLayer;
    };

    self.getScale = function() {
        return scale;
    };

    self.setScale = function(newScale) {
        scale = newScale;
    };

    self.isDraggingAnchor = function() {
        return draggingAnchor;
    };

    self.redraw = function() {
        if (typeof layer.d0x != "undefined" && typeof layer.d0y != "undefined" && typeof layer.d1x != "undefined" && typeof layer.d1y != "undefined" && typeof layer.d2x != "undefined" && typeof layer.d2y != "undefined" && typeof layer.d3x != "undefined" && typeof layer.d3y != "undefined") {
            anchors.length = 0;
            drawDragAnchor(layer.d0x - options.anchorPadding, layer.d0y - options.anchorPadding);
            drawDragAnchor(layer.d1x + options.anchorPadding, layer.d1y - options.anchorPadding);
            drawDragAnchor(layer.d2x + options.anchorPadding, layer.d2y + options.anchorPadding);
            drawDragAnchor(layer.d3x - options.anchorPadding, layer.d3y + options.anchorPadding);
        } else if (typeof layer.x != "undefined" && typeof layer.y != "undefined" && layer.width && layer.height) {
            drawDragAnchor(layer.x + layer.width + options.anchorPadding, layer.y + layer.height + options.anchorPadding);
        }
    };

    var draggingAnchor,
        draggingAnchorIndex,
        draggingStartX,
        draggingStartY;

    self.hitAnchor = function(x, y) {
        var anchor;
        $.each(anchors, function(i, a) {
            var dx, dy, rr;

            // top-left
            dx = x - a.x;
            dy = y - a.y;
            rr = resizerRadius * resizerRadius;
            if (dx * dx + dy * dy <= rr) {
                anchor = a;
            }
        });

        return anchor;
    };

    function handleMouseDown(e) {
        canvasOffset = $(canvas).offset();
        offsetX = canvasOffset.left;
        offsetY = canvasOffset.top;

        var startX = parseInt(e.clientX - offsetX) / self.getScale();
        var startY = parseInt(e.clientY - offsetY) / self.getScale();
        
        draggingAnchor = self.hitAnchor(startX, startY);
        if (draggingAnchor) {
            draggingAnchorIndex = anchors.indexOf(draggingAnchor);
            draggingStartX = startX;
            draggingStartY = startY;
        }
    }

    function handleMouseMove(e) {
        if (draggingAnchor) {
            var mouseX = parseInt(e.clientX - offsetX) / self.getScale();
            var mouseY = parseInt(e.clientY - offsetY) / self.getScale();
            var dx = mouseX - draggingStartX;
            var dy = mouseY - draggingStartY;
            if (typeof layer.d0x != "undefined" && typeof layer.d0y != "undefined" && typeof layer.d1x != "undefined" && typeof layer.d1y != "undefined" && typeof layer.d2x != "undefined" && typeof layer.d2y != "undefined" && typeof layer.d3x != "undefined" && typeof layer.d3y != "undefined") {
                layer["d" + draggingAnchorIndex + "x"] += dx;
                layer["d" + draggingAnchorIndex + "y"] += dy;
            } else if (typeof layer.x != "undefined" && typeof layer.y != "undefined" && layer.width && layer.height) {
                layer.width += dx;
                layer.height += dy;
            }

            draggingStartX = mouseX;
            draggingStartY = mouseY;


            if (options.onDragging) {
                options.onDragging();
            }
        }
    }

    function handleMouseUp(e) {

        if (draggingAnchor && options.onDraggingStopped) {
            options.onDraggingStopped();
        }
        draggingAnchor = null;
    }

    function handleMouseOut(e) {
        handleMouseUp(e);
    }

    $(canvas).mousedown(function(e) { handleMouseDown(e); });
    $(canvas).mousemove(function(e) { handleMouseMove(e); });
    $(canvas).mouseup(function(e) { handleMouseUp(e); });
    $(canvas).mouseout(function(e) { handleMouseOut(e); });

    self.destroy = function() {
        $(canvas).off('mousedown');
        $(canvas).off('mousemove');
        $(canvas).off('mouseup');
        $(canvas).off('mouseout');

        canvas = null;
        context = null;
        layer = null;
    };
};