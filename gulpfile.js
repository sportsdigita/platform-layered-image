var gulp = require('gulp');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat-util');
var clean = require('gulp-clean');

var javascriptFiles = [
    './src/perspective.js',
    './src/resizable-image.js',
    './src/layered-image.js',
    './src/platform-layered-image.js',
    './src/platform-layered-image-editor.js'
];

function build() {
    return gulp.src(javascriptFiles)
        .pipe(concat('platform-layered-image.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./dist'));
}

function runClean() {
    return gulp.src(['./dist'], { read: false, allowEmpty: true })
        .pipe(clean());
}

exports.build = gulp.series(runClean, build);